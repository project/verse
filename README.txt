Projectverse is an installation profile for creating a Drupal-based project management extranet. It currently uses Drupal core, CCK, Views, Organic Groups, Case Tracker and other helper modules to create a groupware environment.

Most of the functionality and design philosophy behind Projectverse is drawn from an existing groupware site (code-named Project Central) that has evolved over time to meet the needs of Drupal services firms and their clients.

Projectverse is sponsored by Exaltation of Larks. Feedback and contributors welcome.